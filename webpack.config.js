const path = require('path');
const ForkTsCheckerWebpackPlugin = require('fork-ts-checker-webpack-plugin');

const ENTRYPOINT = path.resolve(__dirname, 'src');
const DESTINITION = path.resolve(__dirname, 'dist');

module.exports = function build(env, arg) {
  let extra = arg.mode === 'development' ?
    { devtool: 'inline-source-map'}
    : {};
  
  return {
    ...extra,
    mode: arg.mode,
    entry: {
        'payments-gateway': ENTRYPOINT +'/payment.ts'
    },
    output: {
        filename: '[name].js',
        path: DESTINITION,
    },
    resolve: {
        extensions: ['.ts', '.tsx', '.js', '.json']
    },
    module: {
        rules: [
          {
            test: /\.tsx?$/,
            loader: 'ts-loader',
            exclude: /node_modules/,
            options: {
              transpileOnly: true
            }
          }
        ]
    },
    plugins: [
        new ForkTsCheckerWebpackPlugin(),
    ],
};
}
