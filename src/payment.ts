import { setMercadoPago } from '../src/mercadopago/setmercadopago';
import { setDefaultForm } from './setDefaultForm';

declare global {
  interface Window { payments: PaymentsInfo;}
}


function setUpForm() {

  const paymentForms = Array.from(document.querySelectorAll<HTMLFormElement>('form.form-payment-gateway-card'));

  if (paymentForms.length === 0) throw new Error("Payment forms not found");

  for (const payments of paymentForms) {
    try {
      const gateway_id = parseInt(payments.dataset.gateway as string);
      if (typeof gateway_id !== 'number') {
        throw new Error("Ivalid Gateway");
      }
      switch (gateway_id) {
        case 20: setMercadoPago(payments);break;
        default: setDefaultForm(payments);break
      }
    } catch (err) {
      console.error(err);
    }
  }
}

setUpForm();