export const setDefaultForm = (form: HTMLFormElement) => {
  const submitButton = form.querySelector<HTMLButtonElement>('button#payment-submit-button');
  const cardNumberField = form.querySelector<HTMLInputElement>('input#cardNumber');

  submitButton?.addEventListener('click', function(e: Event) {
    e.preventDefault();
    form.submit();
  });

  // TODO - Criar funcão para identificar a bandeira.
  cardNumberField?.addEventListener('blur', function(_e) {
    return true
  });
}