interface IndentificationTypes {
  id: string;
  name: string;
  type: string;
  min_length: number;
  max_length: number;
}

type paymentMethodsParams = {
  bin: string,
  processingMode?: 'aggregator' | 'gateway';
}
type PaymentMethdosResultsSettings = {
  security_code: {
    mode: string;
    card_location: string;
    length: number
  }
  card_number: {
    length: number;
    validation: string
  }
  bin: {
    pattern: string;
    installments_pattern: string;
    exclusion_pattern: string;
  }
}

type PaymentMethdosResults = {
  secure_thumbnail: string;
  min_accreditation_days: number;
  max_accreditation_days: number;
  id: string;
  payment_type_id: string;
  accreditation_time: number;
  thumbnail: string;
  marketplace: string;
  deferred_capture: string;
  labels: string[];
  name: string;
  site_id: string;
  processing_mode: string;
  additional_info_needed: string[];
  status: string;
  settings: PaymentMethdosResultsSettings[]
}

interface PaymentMethods {
  paging: {
    total: number;
    limit: number;
    offset: number;
  }
  results: PaymentMethdosResults[];
}

type cardTokenParams = {
  cardNumber?: string;
  cardholderName?: string;
  cardExpirationMonth?: string;
  cardExpirationYear?: string;
  securityCode: string;
  identificationType?: string;
  identificationNumber?: string;
  cardId?: string
}

type paymentInstallmentsParams = {
  amount: string;
  locale?: string;
  bin: string;
  processingMode?: 'aggregator' | 'gateway';
}

interface PayerCostInstallments {
  installments: number,
  installment_rate: number,
  discount_rate: number,
  labels: string[],
  installment_rate_collector: string[],
  min_allowed_amount: number,
  max_allowed_amount: number,
  recommended_message: string,
  installment_amount: number,
  total_amount: number,
  payment_method_option_id: string
}
interface PaymentInstallmentsResponse {
  merchant_account_id?: string;
  payment_type_id: "credit_card" | 'debit_card';
  payer_costs: PayerCostInstallments[];
}

interface createCardTokenResponse {
  [key: string]: string;
  id: string;
}

declare class MercadoPago {

  constructor(publicKey: string, options?: { locale: string; advancedFraudPrevention: boolean });

  getIdentificationTypes(): Promise<IndentificationTypes[]>;
  getPaymentMethods(paymentMethodsParams: paymentMethodsParams): Promise<PaymentMethods>;
  getIssuers(): Promise<any>;
  getInstallments(paymentInstallmentsParams: paymentInstallmentsParams): Promise<PaymentInstallmentsResponse[]>;
  createCardToken(cardTokenParams: cardTokenParams): Promise<createCardTokenResponse>;
  cardForm(): Promise<any>;
}