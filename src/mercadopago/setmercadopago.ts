import { addErrorMessage, isReadyToSubmit } from '../utils';
import { mercadoPagosetCardBrand } from './mercado-pago-set-brand';
import { mercadoPagoSubmit } from './mercado-pago-submit';

export function setMercadoPago(form: HTMLFormElement) {
  const submitButton = form.querySelector<HTMLButtonElement>('button#payment-submit-button');
  const cardNumberField = form.querySelector<HTMLInputElement>('input#cardNumber');
  const tokenInput = document.createElement('input');
  tokenInput.type = 'hidden'; tokenInput.name = tokenInput.id = 'token';
  form.querySelector('fieldset.master')?.append(tokenInput);

  const mercadoPagoPublicKey = window.payments.MP_PUBLIC_KEY;
  if (!mercadoPagoPublicKey) {
    addErrorMessage(form, 'Não é possivel efetuar pagamentos no cartão no momento', 10000);
    throw new Error('Não é possivel efetuar pagamentos no cartão no momento');
  }
  const mp = new MercadoPago(mercadoPagoPublicKey);

  submitButton?.addEventListener('click', async (e: Event) => {
    e.preventDefault();
    await mercadoPagoSubmit(form, mp);
    if (!tokenInput.value || !isReadyToSubmit(form)) {
      addErrorMessage(form, 'Preencha todas os campos', 10000);
      return;
    }
    form.submit();
  });

  cardNumberField?.addEventListener('blur', async () => {
    await mercadoPagosetCardBrand(form, mp);
  });
}