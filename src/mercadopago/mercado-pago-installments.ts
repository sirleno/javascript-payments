import { removeAllChildNodes } from '../utils'; 
export const mercadoPagoSetInstallments = async (paymentForm: HTMLFormElement, mp: MercadoPago): Promise<void> => {
  const cardNumberInput = paymentForm.querySelector<HTMLInputElement>('input#cardNumber');
  const cardNumber = cardNumberInput?.value.replace(/\s/g, '');
  const bin =  cardNumber?.substring(0,6);
  const amount = paymentForm.querySelector<HTMLInputElement>("input#total_value")?.value;
  const cardInstallmentsHtml = paymentForm.querySelector("select#cardInstallments");
  if (amount && bin) {
    const installmentsResponse = await mp.getInstallments({
      amount,
      bin
    });
    installmentsResponse.forEach(installment => {
      if (installment.payment_type_id === 'credit_card') {
        if (cardInstallmentsHtml) {
          removeAllChildNodes(cardInstallmentsHtml);
          installment.payer_costs.forEach(cost => {
            const option = document.createElement("option");
            const value = cost.installments + "_" + cost.total_amount.toFixed(2);
            option.value = value; option.innerHTML = cost.recommended_message;
            cardInstallmentsHtml.appendChild(option);
          });
        }
      }
    });
  }
}
