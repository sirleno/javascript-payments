import { addErrorMessage, clearField } from '../utils';
import { mercadoPagoSetInstallments } from './mercado-pago-installments';

export const mercadoPagosetCardBrand = async (paymentForm: HTMLFormElement, mp: MercadoPago): Promise<void> => {
  const cardNumberInput = paymentForm.querySelector<HTMLInputElement>('input#cardNumber');
  if (!cardNumberInput || !cardNumberInput.value) return;
  const cardNumber = cardNumberInput.value.replace(/\s/g, '');
  if (cardNumber.length < 6) return;
  const response = await mp.getPaymentMethods({ bin: cardNumber.substring(0,6)});
  if (!response.results || !response.results[0] || !response.results[0].id) {
    addErrorMessage(paymentForm, 'Cartão Inválido');
    clearField(cardNumberInput);
  }
  const payment_method_id = response.results[0].id;
  const paymentMethod = paymentForm.querySelector<HTMLInputElement>('input#payment_method');
  if (paymentMethod) {
    paymentMethod.value = payment_method_id;
  }
  
  await mercadoPagoSetInstallments(paymentForm, mp);
}