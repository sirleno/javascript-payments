import { validateGRecaptcha, mapFormData } from '../utils';

export const mercadoPagoSubmit = async (paymentForm: HTMLFormElement, mp: MercadoPago): Promise<void> => {
  try {
    await validateGRecaptcha(paymentForm);
    const formData = mapFormData(paymentForm);
    const cardholderName = formData.get('cardHolderName') as string;
    const cardNumber = formData.get('cardNumber')?.replace(/\s/g, "") as string;
    const securityCode = formData.get('cardSecurityCode') as string;
    const expirationDate = formData.get('cardExpirationDate') as string;
    const [ cardExpirationMonth, cardExpirationYear ] = expirationDate.split('/');

    const token = await mp.createCardToken({
      cardNumber,
      cardholderName,
      securityCode,
      cardExpirationMonth,
      cardExpirationYear
    });

    if (!token.id) throw new Error('Failed to generate card token');
    const tokenInput = paymentForm.querySelector<HTMLInputElement>('input#token');
    if (tokenInput) tokenInput.value = token.id;
  } catch (err) {
    console.error(err);
  }
}
