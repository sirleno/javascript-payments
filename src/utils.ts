const mapFormData = (form: HTMLFormElement): Map<string, string> => {
  const formData = new FormData(form);
  const map = new Map<string,string>();
  formData.forEach((element, index) => map.set(index, <string>element));
  return map;
}

const addErrorMessage = (form: HTMLFormElement, errorMessage: string, time: number = 4000): void => {
  const alertField = form.querySelector<HTMLElement>('div.alert-danger.main-alert');
  if (alertField) {
    alertField.innerHTML = errorMessage;
    alertField.classList.remove('displaynone');
    setTimeout(() => {
      alertField.innerHTML = '';
      alertField.classList.add('displaynone');
    }, time);
  }
}

const clearField = (field: HTMLInputElement) => {
  field.innerHTML = '';
  field.value = '';
}

const validateGRecaptcha = async (form: HTMLFormElement): Promise<void> => {
  const rInput = form.querySelector<HTMLInputElement>('input#googleRecaptcha');
  if (!rInput) throw new Error('Missing Recaptcha Input');
  const site_key = rInput?.dataset.sitekey;
  const action = rInput?.dataset.action;
  if (!site_key || !action) throw new Error('No site recpatcha site key or action name');
  grecaptcha.ready(async () => {
    const token = await grecaptcha.execute(site_key, { action });
    rInput.value = token;
  });
}

const isReadyToSubmit = (form: HTMLFormElement): boolean => {
  const cardNumber = form.querySelector<HTMLInputElement>("input#cardNumber")?.value;
  const cardExpirationDate = form.querySelector<HTMLInputElement>("input#cardExpirationDate")?.value;
  if (
    cardNumber && isValidCreditCardWithLuhn(cardNumber) &&
    cardExpirationDate && isValidCardExpirationDate(cardExpirationDate)
    ) {
    return true;
  }
  return false;
}

function isValidCardExpirationDate(value: string): boolean {
  const [ monthString, yearStrign ] = value.split("/");
  const month = parseInt(monthString);
  const year = parseInt(yearStrign);

  if (month < 1 || month > 12) {
    throw new Error("Mês inválido");
  }
  if (year < (new Date()).getFullYear()) {
    throw new Error("Ano Inválido inválido");
  }
  return true;
}

function isValidCreditCardWithLuhn(value: string): boolean {
	if (/[^0-9-\s]+/.test(value)) return false;
	// The Luhn Algorithm. It's so pretty.
	let nCheck = 0, bEven = false;
	value = value.replace(/\D/g, "");

	for (var n = value.length - 1; n >= 0; n--) {
		var cDigit = value.charAt(n),
			  nDigit = parseInt(cDigit, 10);

		if (bEven && (nDigit *= 2) > 9) nDigit -= 9;

		nCheck += nDigit;
		bEven = !bEven;
	}

	return (nCheck % 10) == 0;
}

function removeAllChildNodes(element: Element): void {
  while(element.firstChild) {
    element.removeChild(element.firstChild);
  }
}

export { mapFormData, addErrorMessage, clearField, validateGRecaptcha, isReadyToSubmit, removeAllChildNodes };
